#!/bin/bash

package_list="${@}"
random_name=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo '')

#===============================================================================
#                                                                              #
#    Funções para gerar mensagens pré formatadas                               #
#                                                                              #
#===============================================================================
                                                                               #
function show_error() {                                                        #
  echo -e "\033[01;31m\nErro:\033[00m $1 \n      $2\n\n      $3\n"             #
  exit 1                                                                       #
}                                                                              #
                                                                               #
function show_warning() {                                                      #
  echo -e "\033[01;32m$1\033[00m\n\n\033[01;34mAviso: \033[00m$2\n"            #
  exit 0                                                                       #
}                                                                              #
                                                                               #
function show_message() {                                                      #
  echo -ne "$1 \033[01;33m$2\033[00m"                                          #
}                                                                              #
                                                                               #
function show_message_bold() {                                                 #
  echo -ne "\n\033[01m$1\033[00m$2"                                            #
}                                                                              #
                                                                               #
function check_task() {                                                        #
  echo -e "\033[01;32mFeito!\033[00m"                                          #
}                                                                              #
                                                                               #
function ask() {                                                               #
  echo -ne "\033[37;44m $1 \033[00m\033[34m▶\033[00m "                        #
  read -n1 RESPOSTA                                                            #
  RESPOSTA=$(echo -n ${RESPOSTA^^})                                            #
}                                                                              #
                                                                               #
#==============================================================================#

function translate_package(){
  local translated=$(grep " ${1}"$ ${APK_CACHE_FILE} | cut -d\: -f1 2> /dev/null)
  [ "${translated}" = "" ] && {
    echo ${1}; return
  }
  echo ${translated}
}

function update_cache(){
  APK_CACHE_FILE=/var/cache/apt/pkgconfig.bin
  [ ! -f "${APK_CACHE_FILE}" ] && {
    show_message "Generating" "pkgconfig "
    echo -n "database... "
    local pkg_config_path="/usr/lib/x86_64-linux-gnu/pkgconfig/"
    apt-file $APT_OPTIONS search ${pkg_config_path} | sed "s|${pkg_config_path}||g" | sed 's/...$//' > ${APK_CACHE_FILE}
    check_task
  }
  unset package_list
}

function check_root() {
  [ $EUID != 0 ] && {
    show_error  "Você precisa de permissões de super usuário."  \
                "reexecute o comando com sudo:"                 \
                "sudo ${0} ${package_list}"
  }
}

function download_package() {
  url_to_download="$1"
  file_name=$(echo "$url_to_download" | sed 's/^/basename /g' | sh)
  package_display_name=$(echo "$file_name" | sed 's/_/ /g' | awk '{print $1}')

  show_message "Baixando o pacote " "$package_display_name\n"
  wget -q -c --progress=bar:force --show-progress "$1" -O - -o /dev/null > $file_name
  echo
}

function confirm_packages_download() {
  check_task
  show_message      "\nO(s) seguinte(s) pacote(s) precisa(m) ser baixado(s):\n"
  show_message      "" "\n$PACKAGE_NAMES\n"
  show_message_bold "Deseja prosseguir?" "(S/N)\n\n"
  ask               "Sua resposta"

  [ "$RESPOSTA" = "S" ] && return 0

  show_message_bold "\nCancelando a instalação" " do(s) pacote(s)... "
  check_task         
  echo                                                   
  exit 1
}

function check_for_existence() { 
  avaialable_packages=$(apt-cache pkgnames)
  for package in "${package_list[@]}"; do
    show_message "Verificando a existência do(s) pacote(s)" "${package} "
    existe=$(echo "${avaialable_packages}" | grep '^'$package'$')
    [ "$existe" = "" ] && packages_not_found+="$package "
    check_task
  done

  echo

  [ "$packages_not_found" = "" ]                           ||  
  show_error "O(s) seguinte(s) pacote(s):\n"               \
             "\033[01;33m$packages_not_found\033[00m"      \
             "não existe(m) nos repositórios do sistema"   ##

  
}

function mark_as_auto_installed(){
  apt-mark auto $PACKAGE_NAMES > /dev/null
  apt-mark manual ${@} > /dev/null
}

function get_package_list() {
  show_message "Obtendo a lista dos pacotes a serem instalados..."
  APT_OUTPUT=$(apt install ${@} --print-uris 2>&1)
  DOWNLOAD_URLS=$(echo "$APT_OUTPUT" | cut -d "'" -f 2 | grep -e "^http")

  [ "$DOWNLOAD_URLS" = "" ] && \
  show_warning "Feito! " "O(s) pacote(s) já está(ão) instalado(s) e na sua última versão"

  FILE_NAMES=$(echo "$DOWNLOAD_URLS" | sed 's/^/basename /g' | sh)
  PACKAGE_NAMES=$(echo "$FILE_NAMES" | sed 's/_/ /g' | awk '{print $1 " "}')
}

function do_download_work() {
  mkdir -p /var/cache/apt/archives/tmp.$random_name
  cd /var/cache/apt/archives/tmp.$random_name 

  echo -e "\n"

  while read -r package_url; do
    download_package "$package_url"
  done <<< "$DOWNLOAD_URLS"

  show_message_bold "Arquivo(s) baixado(s)! " "Iniciando a instalação...\n\n"
  dpkg -i ./*.deb
}

function finish_work(){
  cd ~
  rm -rf "/var/cache/apt/archives/tmp.$random_name"
  show_message_bold "Finalizando a instalação... "
}






check_root
update_cache

for package in "${@}"; do
  package_list+=($(translate_package "${package}"))
done

check_for_existence
get_package_list ${package_list[@]}
confirm_packages_download
do_download_work
finish_work
mark_as_auto_installed ${package_list[@]}
check_task
echo

















