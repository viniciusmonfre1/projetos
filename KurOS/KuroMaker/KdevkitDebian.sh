#!/bin/sh
echo "
#########################################################################
#                  _  __     ____             _  ___ _                  #
#                 | |/ /    |  _ \  _____   _| |/ (_) |_                #
#                 | ' /_____| | | |/ _ \ \ / / ' /| | __|               #
#                 | . \_____| |_| |  __/\ V /| . \| | |_                # 
#                 |_|\_\    |____/ \___| \_/ |_|\_\_|\__|               #
#                                                                       #
#                                                                       #
# Criado Por: Vinicius Monfre                                           #
#                                                                       #
# Versão KurOS 1.2                                                      #
#                                                                       #
#        K-Devkit é um software para desenvolvimento do KurOS           #
#                                                                       #
#########################################################################
"



sleep 5  


sudo mkdir /work
cd /work

echo "instalando os pacotes de linguagem em portugues"
sleep 5
sudo apt install *l10n-pt-br *pt-br

echo "Configurando repositórios"
sleep 5
sudo rm /etc/apt/sources.list
sudo echo "#------------------------------------------------------------------------------#
#                   OFFICIAL KurOS REPOS                    
#------------------------------------------------------------------------------#

###### Debian Main Repos
deb http://ftp.br.debian.org/debian/ testing main contrib non-free
deb-src http://ftp.br.debian.org/debian/ testing main contrib non-free

deb http://ftp.br.debian.org/debian/ testing-updates main contrib non-free
deb-src http://ftp.br.debian.org/debian/ testing-updates main contrib non-free

deb http://security.debian.org/ testing-security main
deb-src http://security.debian.org/ testing-security main " > /etc/apt/sources.list

sudo apt update
sudo apt install zram* preload prelink wget
sudo rm -r /usr/share/backgrounds; sudo mkdir /usr/share/wallpapers; cd /usr/share/; sudo wget https://sourceforge.net/projects/tardisdevs/files/kuros/wallpapers.tar.gz ; sudo tar -vzxf wallpapers.tar.gz; sudo rm wallpapers.tar.gz; sudo wget https://sourceforge.net/projects/tardisdevs/files/kuros/pixmaps.tar.gz ; sudo tar -vzxf pixmaps.tar.gz; sudo rm pixmaps.tar.gz;
cd /  ; sudo wget https://sourceforge.net/projects/tardisdevs/files/kuros/scripts.tar.gz; sudo tar -vzxf scripts.tar.gz ; sudo rm scripts.tar.gz ;
cd /etc ; sudo wget https://sourceforge.net/projects/tardisdevs/files/kuros/skel.tar.gz ; sudo tar -vzxf skel.tar.gz ; sudo rm skel.tar.gz ; cd skel ; 

echo "Instalando Core"
sleep 5  
sudo apt install sddm kde-plasma-desktop plasma-nm bash-completion nano sudo ark rar unrar kcalc okular gwenview kolourpaint4 vlc htop iotop nethogs iftop locate traceroute mtr whois nmap gtk3-engines-breeze wireless-tools dirmngr net-tools dnsutils apt-transport-https firmware-linux


echo "Instalando extra"
sleep 5  
sudo apt install firefox-esr geany gimp cups libreoffice screenfetch p7zip zip telegram-desktop
cd /work
echo "Instalando Drivers"
sleep 5  
sudo apt install firmware-linux-free firmware-linux-nonfree atmel-firmware bluez-firmware firmware-b43-installer firmware-b43legacy-installer firmware-bnx2 firmware-bnx2x firmware-brcm80211 firmware-intelwimax firmware-ipw2x00 firmware-ivtv firmware-iwlwifi firmware-libertas firmware-myricom firmware-netxen firmware-qlogic firmware-ralink firmware-realtek intel-microcode libertas-firmware zd1211-firmware firmware-linux-free firmware-linux-nonfree firmware-atheros

cd /etc/skel && sudo cp -r .* ~/ && cp -r .* ~/

rm -r /work
