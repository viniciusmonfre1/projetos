 
#!/bin/bash
########################################################################
# Edited By: Vinicius Monfre                                           #
#                                                                      #
# Version 1.0                                                          #
# Original Author: Carlos Fagiani Junior                               #
# E-mail: fagianijunior@gmail.com                                      #
# Este é um Script de instalação do KurOS versão archlinux             #
#                                                                      #
########################################################################

# 1- Rode o CD/PenDrive de instalação do Arch Linux
# 2- Transfira o script para a máquina (via PenDrive ou via wget)
# 3- De permissão e rode o Script
# chmod +x installKuros.sh
# ./archinstall.sh
# Se tudo ocorrer bem, seu KurOS estará instalado.

##Essas informações podem ser alteradas
root_senha="kuros";

layout_teclado="br-abnt2"; #us
linguagem="pt_BR.UTF-8";
font="lat9w-16";
font_map="8859-1_to_uni";

hostname="KurOS";
localtime="America/Fortaleza";

boot_hd="/dev/sda1";
swap_hd="/dev/sda2";
root_hd="/dev/sda3";
home_hd="/dev/sda4";

# nao/sim
formatar_home_hd="nao";

# Usar wifi na instalação? sim/nao
usar_wifi="nao";

# syslinux/grub/nenhum
boot_loader="grub";

#sim/nao #Está opção ainda não funciona
#pos_instalacao="sim";

alterei_os_dados_acima="nao";
####################################
# Não alterar a partir deste ponto #
####################################
function espera() {
	read -p "$1 Tecle <ENTER> para continuar..." a;
	unset a;
}

loadkeys $layout_teclado;
espera "Teclado Configurado.";

# Verifica se as informações estão corretas #

if [ "$alterei_os_dados_acima" == "nao" ]; then
   echo "Antes de rodar o script edite os dados internos. Operação cancelada.";
   exit;
fi

if [ ! -e "$boot_hd" ] || [ ! -e "$swap_hd" ] || [ ! -e "$root_hd" ] || [ ! -e "$home_hd" ]; then
   echo "Crie as 4 partições antes de continuar com o script.";
   echo "Será iniciado o comando 'cfdisk' para isso.";
   echo "A primeira partição deve ser a boot (~100MB)";
   echo "A segunda partição deve ser a SWAP (~1024MB)";
   echo "A terceira partição deve ser a ROOT (>=3GB)";
   echo "A quarta partição deve ser a HOME (Tamanho variado >=3GB)";
   espera "cfdisk";
   cfdisk;
   if [ ! -e "$boot_hd" ] || [ ! -e "$swap_hd" ] || [ ! -e "$root_hd" ] || [ ! -e "$home_hd" ]; then
      espera "Particionamento errado, saindo do script";
      exit;
   fi
fi

mkfs -t ext2 $boot_hd;
espera "$boot_hd formatado.";
mkswap $swap_hd;
espera "Swap criada em $swap_hd.";
mkfs -t ext3 $root_hd;
espera "$root_hd formatado.";

if [ "$formatar_home_hd" == "sim" ]; then
   mkfs -t ext3 $home_hd;
   espera "$home_hd formatado.";
fi

swapon $swap_hd;
espera "SWAP ligada.";
mount $root_hd /mnt;
espera "$root_hd montado em /mnt";
mkdir /mnt/{boot,home};
espera "Pasta /mnt/boot e /mnt/home criados.";
mount $boot_hd /mnt/boot;
espera "$boot_hd montado em /mnt/boot";
mount $home_hd /mnt/home;
espera "$home_hd montado em /mnt/home";

if [ "$usar_wifi" == "sim" ]; then
	wifi-menu;
	espera "Wifi conectado.";
fi

pacstrap /mnt base base-devel wpa_supplicant dialog;
espera "base e base-devel instalados.";

if [ "$boot_loader" == "grub" ]; then
	pacstrap /mnt grub-bios;
elif [ "$boot_loader" == "syslinux" ]; then
	pacstrap /mnt syslinux gptfdisk;
fi
espera "$boot_loader bootloader instalado.";

genfstab -p /mnt >> /mnt/etc/fstab;
cat /mnt/etc/fstab;
espera "fstab gerado.";

echo $hostname > /mnt/etc/hostname;
espera "Adicionou $hostname em /etc/hostname";

arch-chroot /mnt /bin/bash -c "ln -s /usr/share/zoneinfo/$localtime /etc/localtime;";
espera "Configurou localtime.";

echo "LANG="$linguagem > /mnt/etc/locale.conf;
espera "Criou o arquivo locale.gen.";

echo "KEYMAP="$layout_teclado > /mnt/etc/vconsole.conf;
echo "FONT="$font >> /mnt/etc/vconsole.conf;
echo "FONT_MAP="$font_map >> /mnt/etc/vconsole.conf;
espera "Configurou o vconsole.conf.";

cp /mnt/etc/locale.gen /mnt/tmp/locale.gen;
sed "s/#"$linguagem"/"$linguagem"/g" /mnt/tmp/locale.gen > /mnt/etc/locale.gen;

arch-chroot /mnt /bin/bash -c "locale-gen";
arch-chroot /mnt /bin/bash -c "mkinitcpio -p linux";
espera "gerou o locale. Criou a RAM disk.";

if [ "$boot_loader" == "grub" ]; then
   arch-chroot /mnt /bin/bash -c "modprobe dm-mod";
   arch-chroot /mnt /bin/bash -c "grub-install – –target=i386-pc – –recheck /dev/sda "${boot_hd:0:8};
   arch-chroot /mnt /bin/bash -c "mkdir -p /boot/grub/locale";
   arch-chroot /mnt /bin/bash -c "cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo";
   arch-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg";
elif [ "$boot_loader" == "syslinux" ]; then
   arch-chroot /mnt /bin/bash -c "/usr/sbin/syslinux-install_update -iam;";
fi
espera "Configurou o $boot_loader";



arch-chroot /mnt /bin/bash -c "passwd << EOF
$root_senha
$root_senha
EOF";
espera "Setou a senha do ROOT.";


#Instalando o desktop e configurando o mesmo
espera "Instalando o desktop"
arch-chroot /mnt /bin/bash -c "pacman -S sudo kdebase plasma sddm sddm-kcm networkmanager networkmanager-qt nm-connection-editor git "
arch-chroot /mnt /bin/bash -c "systemctl enable sddm"
arch-chroot /mnt /bin/bash -c "systemctl enable NetworkManager"
arch-chroot /mnt /bin/bash -c "rm -r /usr/share/backgrounds; mkdir /usr/share/wallpapers; cd /usr/share/wallpapers; wget http://download2263.mediafire.com/mjhxckvpdazg/k2m6krmh7p9slna/mosaic_KurOS.png ; wget http://download2265.mediafire.com/8hguc6g1e4sg/a9cur4zu0jf4lqf/Kur_Universe.png ; wget http://download2265.mediafire.com/i4a9m6sljljg/1rjbl1ak8viddhm/bluekuro.png "
arch-chroot /mnt /bin/bash -c "cd /usr/share/pixmaps ; wget http://download1336.mediafire.com/bm92sneqdngg/tqrn1oczg0zs6z8/kuros1.svg ; mkdir /scripts; cd /scripts ; wget http://download938.mediafire.com/hkq56xsjzcmg/pldz3bhcm16a4i8/gato.txt ; "
arch-chroot /mnt /bin/bash -c "pacman -S  acpi acpid akonadi-calendar-tools akregator alsa-firmware alsa-utils ark aspell-en audiocd-kio bash-completion binutils bluedevil breeze-gtk cryfs discover dolphin-plugins dragon drkonqi ffmpegthumbs filelight fprintd frei0r-plugins grantlee-editor gst-libav gst-plugins-ugly gwenview intel-media-driver jre8-openjdk juk k3b kaddressbook kalarm kamera kamoso kate kbackup kcalc kcharselect kcolorchooser kcron kde-gtk-config kdebugsettings kdegraphics-mobipocket kdenetwork-filesharing kdepim-addons kdeplasma-addons kdf kdialog kfind kfloppy kgamma5 kget khelpcenter kinfocenter kleopatra kmag kmail kmousetool kmouth knotes kolourpaint kompare kontact konversation korganizer krdc krfb kross-interpreters kruler kscreen ksshaskpass ksystemlog kteatime ktimer kwallet-pam kwalletmanager kwave kwayland-integration kwrite kwrited libva-intel-driver libva-mesa-driver libva-utils libva-vdpau-driver lsb-release mesa-demos mesa-vdpau networkmanager-openconnect networkmanager-openvpn networkmanager-pptp networkmanager-vpnc nss-mdns numlockx okular os-prober packagekit-qt5 partitionmanager pavucontrol-qt plasma-browser-integration plasma-desktop plasma-nm plasma-pa plasma-vault plasma-workspace-wallpapers powerdevil print-manager pulseaudio-bluetooth pulseaudio-zeroconf samba sddm-kcm skanlite spectacle sweeper system-config-printer telepathy-kde-approver telepathy-kde-auth-handler telepathy-kde-call-ui telepathy-kde-contact-list telepathy-kde-contact-runner telepathy-kde-desktop-applets telepathy-kde-filetransfer-handler telepathy-gabble telepathy-haze telepathy-idle telepathy-kde-integration-module telepathy-kde-send-file telepathy-kde-text-ui telepathy-salut user-manager vdpauinfo virtualgl xdg-desktop-portal-kde xdg-user-dirs xf86-input-elographics xf86-input-evdev xf86-input-void xf86-input-wacom xf86-video-amdgpu xf86-video-ati xf86-video-nouveau xf86-video-vesa xorg-fonts-100dpi xorg-fonts-75dpi xorg-mkfontscale xorg-server xorg-server-xwayland xorg-twm xorg-xdpyinfo xorg-xdriinfo xorg-xgamma xorg-xhost xorg-xinit xorg-xinput xorg-xkill xorg-xlsclients xorg-xrefresh xorg-xvinfo xorg-xwininfo yakuake zeroconf-ioslave firefox geany gimp cups-pdf cups-pk-helper calligra amdvlk vulkan-intel vulkan-mesa-layer vulkan-radeon vulkan-tools cpio cpupower dmidecode exa git hddtemp hwdetect hwinfo neofetch net-tools nmon p7zip pacman-contrib pkgfile powertop pwgen strace tmux tree unrar wireguard* zip zsh-autosuggestions "

arch-chroot /mnt /bin/bash -c " cd /tmp ; wget http://download1638.mediafire.com/uurh76hedb0g/dqxsmo56areyqzq/skel.tar.gz ; tar -vzxf skel.tar.gz ; cd skel ; cp -r * /etc/skel ; cp * /etc/skel "

umount /mnt/{boot,home,};
espera "Desmontou as partições.";


espera "Seu novo KurOS está instalado.";
exit;
