# -*- coding: utf-8 -*-

import logging
import random
from aiogram import Bot, Dispatcher, executor, types




def run_bot(dp):
    @dp.message_handler()
    async def message(message: types.Message):
        if 'acontece' in message.text.lower():
            response, reply = get_random_acontece()
            await message.reply(response, reply=reply)
        
        elif 'loot' in message.text.lower():
            response, reply = get_random_response_loot()
            await message.reply(response, reply=reply)

        elif 'sorte' in message.text.lower():
            response, reply = get_random_response_sorte()
            await message.reply(response, reply=reply)
            



def get_random_acontece():
    possible_responses = [
        ("Você estava andando tranquilamente quando tropeça em um kit médico parcialmente enterrado e ele tem /1d3 usos", 0),
        ("você achou uma pequena bateria no chão", 1),
        ("Você encontrou uma lata de comida", 1),
        ("Você encontrou um isqueiro", 1),
        ("Você encontrou um kit de primeiros socorros", 0),
        ("Você encontrou uma garrafa de agua", 1),
        ("Você estava andando tranquilamente quando encontra /1d20 criaturas", 1),
        ("Você Desenvolveu desinteria", 0),
        ("Você sente dor de cabeça", 1),
        ("Você encontrou comprimido de aspirina", 1),
        ("Você estava andando de boas quando algo pega seu pé. rode um iniciativa", 0),
        ("Você tropeçou e caiu", 1),
        ("Você estava andando junto de seus aliados quando escuta um barulho de rachadura e todos que falharem no teste de destreza caem em um estacionamento subterranio com /1d6 criaturas ", 1),
        ("Você leva /1d4 de dano para uma criatura que te pegou de surpresa",0),
        ("Você começa a sentir tontura",0),
        ("Você se lembrou do jogo",0),
        ("Você estava desprevenido quando tropeça em um loot e leva 1d6 de dano de queda",1),
        ("um pombo fez cocô em você",0),
        ("Você desenvolve tontura acaba batendo a cabeça numa caixa, perca 10 minutos de ações, e rode um loot",0),
        ("Rode um teste de constituição, se falhar você está infectado com o prion",1),
        ("rode um acontece",0),
        ("Aconteceu uma desgraça",0),
        ("Você adquiriu sono",0),
        ("Suas pernas estão cansadas, pare para descansar ou sofra um desmaio.",0),
        ("Aconteceram /1d20 desgraças",0),
        ("Você achou 1d4 velas",0),
        ("Você achou um ventilador perfeitamente funcional",1),
        ("você encontra garrafa de veneno de rato",0),
        ("Você estava andando de boas quando pisa em um escorpião, rode um teste de constituição para não ser envenenado",1),
        ("você encontra um coquetel molotov que da 1d20 de dano de fogo",0),
        ("Bem vindo ao jogo, rode 3 testes de sorte, se 1 deles cair desgraça você é atingido por um raio e leva 1d20 de dano",0),
        ("Você acidentalmente engoliu uma mosca",1),
        ("você encontra uma caixa de madeira com os seguintes itens: 5kg Arroz; 2kg Feijão; Óleo de soja; Sal; Açúcar; 1kg Café; Molho de tomate; Macarrão; Milho; Farinha de trigo; Farinha de mandioca; 6 lts Leite; Manteiga; Chá; 1kg Carnes; 2 Peixes; 8 Ovos; 4 Frutas (Melancia, laranja, pêra, mamão, e tomate); Granola; Suco; Requeijão; Cereais; bolacha;  Biscoito doce; Biscoito Salgado.",0),
        ("Você encontra um cofre, mas onde está a chave?",1),
        ("Você encontra uma chave, mas onde está o cofre?",0),
        ("Você acredita que é um cogumelo agora. perca 1d6 de sanidade",1),
        ("Você encontra um botijão de gás",0),
        ("Você encontra um radio amador mas falta uma pequena bateria",1),
        ("Você levou uma bala perdida, rode 1d6 de dano",0),
        ("Você encontra uma bateria de carro, será que possui eletricidade? Rode um sorte.",1),
        ("você encontra um abrigo anti bombas com suprimento de comida que dura 2 meses, mas não possui água",0),
        ("você acha a cura para a infecção zumbi, mas infelizmente você tropeçou e caiu, jogando toda a formula no chão",1),
        ("Um pombo contaminou sua agua.",0),
        ("Você encontra uma máscara de gás enterrada, rode um consertar.",1),
        ("Você encontrou um travesseiro!",0),
        ("você avista uma loja de roupas abandonada. bom saque",1),
        ("você encontrou um kit de cirurgia com /1d3 usos",0),
        ("você encontrou seu pai, ele voltou da compra do cigarro. ele é um zumbi agora pegue sua pensão!",1),
        ("rode um loot",0),
        ("Você encontrou 1d20 parafusos",0),
        ("Você refletiu sobre sua existência e encontrou o significado da vida, recupere 1d8 de sanidade.",0),
        ("parabéns, você ganhou essa opção que vale 1% do seu dado. você tem muita sorte por ter tirado esse extremo, mas nada acontece. ",1),
        ("Sua mãe te manda pausar o rpg, perca uma ação explicando a ela que rpg não tem pause.",0),
        ("você acha a cura para a infecção zumbi e ela tem 1d3 usos",1),
        ("você acha um pente de balas para revolver",1),
        ("você acha um pente de balas para pistola automática",1),
        ("você acha uma caixa de balas para espingarda, rode um /d20 para ver quantas balas tem",1),
        ("você acha um pente de balas para fuzil",1),
        ("você acha um pente de balas para metralhadora",1),
        ("você acha um pente de balas para revolver",0),
        ("você acha um pente de balas para pistola automática",0),
        ("você acha uma caixa de balas para espingarda /d20 para ver quantas balas tem",0),
        ("você acha um pente de balas para fuzil",0),
        ("você acha um pente de balas para metralhadora",0),
        ("você acha um pente de balas para revolver",1),
        ("você acha um pente de balas para pistola automática",1),
        ("você acha uma caixa de balas para espingarda /d20 para ver quantas balas tem",1),
        ("você acha um pente de balas para fuzil",1),
        ("você acha um pente de balas para metralhadora",1),
        ("ඞ AMOGUS ඞ",0),
        ("você encontra uma garrafa de poty",1),
        ("Você vê! se era cego, agora enxerga",1),
        ("Você encontra uma chapa de metal",1),
        ("Você encontra 1d6 fios de cobre",1),
        ("Você encontra uma pedra com um rosto desenhado nela",1),
        ("Você encontra um pedaço de pano velho",1),
        ("Você está com fome",1),
        ("Você encontra uma lata de sardinha no chão",1),
        ("Você encontrou um pneu de celta 2008 em bom estado",1),
        ("Você encontra um pc antigo quebrado",1),
        ("Você encontra um gerador pequeno com defeito",1),
        ("Vocês estão com frio, se cubram",1),
        ("Vocês estão com calor, se refresquem",1),
        ("Você sente um calafrio, rode um acontece",1),
        ("Você encontrou uma lanterna sem bateria",1),
        ("Você se sente bem, recupere 1d6 de vida",1),
        ("Você encontrou um bilhete premiado da mega sena!, pena q não pode recebê-lo",1),
        ("Você encontra uma bússola que só aponta  pro sul",1),
        ("Você encontra um caderno preto com capa de couro e uma caneta, mas tem seu nome escrito nele",1),
        ("Você encontra uma bíblia satânica com 3 páginas",1),
        ("Você encontra uma foto de família",1),
        ("Você encontra uma bandeira do Corinthians, fuja pois um torcedor fanático está a caminho",1),
        ("Você encontra um crucifixo, mas sempre que cai, cai invertido",1),
        ("Você encontrou uma rede entre duas árvores",1),
        ("Você vê jesus, rode 1d10 para recuperar sanidade",1),
        ("Você encontra o Mohamed, e ele te oferece uma granada ‘Essa vai explodir o zumbi’",1),
        ("Você encontrou uma galinha, mantenha-a alimentada para ela te presentear com ovos",1),
        ("você encontra uma vaca leiteira, rode um teste de sorte para ver se ela está infectada",1),
        ("Você encontra um pacote de sementes mistas (abóbora, tomate, cenoura, repolho, melancia, cebola, feijões, rúcula e alface) inicie sua plantação",1),
        ("Você vê uma galinha, ela está chocando em um ninho, quando se aproxima percebe que ela está com 1d12 pintinhos",1),
        ("Você encontra um porco, rode um teste de carisma",1),
        ("Você encontra uma maleta suspeita, rode um teste de sorte, se cair duas desgraças ela explode dando 1d10 de dano",1),
        ("você vê uma fazenda abandonada, porém ela não possui nenhum animal ou planta, apenas um poço com agua, fortifique-a se quiser que ela se torne sua base",1),
        ("Você encontrou 1d8 tábuas de madeira",1),
        ("Você encontra 1d20 pregos",1),
        ("você avista uma chave de fenda",1),
        ("Você encontra um cobertor",1),
        ("Você encontra um pé de melancia cm 1d4 melancias maduras",1),
        ("Você avista um gnomio que olha em sua direção e grita ‘uh’",1),
        ("Você tem amnésia temporária e esqueceu seu ultimo trauma recupere 1d6 de sanidade",1),
        ("Rode 1 acontece, 1 loot e 1 sorte, se cair desgraça em qualquer um você não encontra nada",1),
        ("Você foi atacado por um pombo, rode um teste de sorte pra ver se foi infectado",1),
        ("Você encontra uma lata de vegetais refogados",1),
        ("cães infernais te cercaram, lute por sua vida",1),
        (" você avista um pequeno grupo de sobreviventes, todos estão armados e parecem hostis, se vira",1),
        ("Você encontra uma criança chorando no canto, o que você faz?",1),
        ("Um cãozinho perdido te segue, o que você faz?",1),
        ("você encontra um filhote de gato ferido miando por ajuda. ",1),
        ("Você encontra um poço, gire um sorte",1),
        ("Você avista  um hospital abandonado. em seu interior existem vários medicamentos úteis para sua sobrevivência, porém também há 1d20 zumbis",1),
        ("olhe, um colete a prova de balas que te da 1d4 de defesa",1),
        ("você encontra 1d4 cup noodles",1),
        ("você encontra 1 miojo sabor frango",1),
    ]
    return random.choice(possible_responses)


def get_random_response_loot():
    possible_responses = [
        ("Você encontrou /1d200 reais e Você encontrou uma arma de fogo", 1),
        ("Você encontrou /1d200 reais e Você encontrou uma faca enferrujada", 0),
        ("Você encontrou /1d200 reais e Você encontrou um facão", 1),
        ("Você encontrou /1d200 reais e Você encontrou um pé de cabra", 1),
        ("Você encontrou /1d200 reais e Você encontrou uma haste de ferro afiada", 1),
        ("Você encontrou /1d200 reais e Você encontrou uma barra de metal", 0),
        ("Você encontrou /1d200 reais e Você encontrou um bastão de madeira", 1),
        ("Você encontrou /1d200 reais e Você encontrou uma velha marreta", 1),
        ("Você encontrou /1d200 reais e Você encontrou rode o comando acontece", 0),
        ("Você encontrou /1d200 reais e surgiram /1d20 criaturas", 1),
        ("Você encontrou /1d200 reais e uma soqueira", 1),
        ("Você encontrou /1d200 reais e aconteceu uma  desgraça", 1),
        ("Você encontrou /1d200 reais e um colete a prova de balas que te dá 1d4 de proteção", 1),

    ]
    return random.choice(possible_responses)


def get_random_response_sorte():
    possible_responses = [
        ("Você está com sorte", 1),
        ("Um azar paira sobre você", 0),
        ("Aconteceu uma desgraça", 1),
        ("Aconteceram DUAS desgraças", 0),
        ("Você está com sorte", 1),
    ]
    return random.choice(possible_responses)

    



	

if __name__ == '__main__':
    API_TOKEN = 'Seu_Token_Aqui'
    logging.basicConfig(level=logging.INFO)
    bot = Bot(token=API_TOKEN)
    dp = Dispatcher(bot)
    run_bot(dp)
    executor.start_polling(dp, skip_updates=True)
