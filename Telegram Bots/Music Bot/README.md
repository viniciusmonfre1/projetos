**Guia de comandos do bot**
 
_Adicionei finalmente os comandos ao bot, segue um guia de como eles funcionam_
 
- **Play + link da musica** : Toca 1 video 

- **Stop** : para o bot
 
- **List + link da musica** : adiciona uma musica na fila de reprodução, mas não toca a mesma
 
- **Run** : toca a fila de reprodução criada em ordem

- **Random** : toca a lista de reprodução em modo shuffle
 
- **Delete** : apaga a lista de reprodução

- **Skip** : pula uma musica na lista
 
- **Last** : volta pra musica anterior

- **OBS:** _os comandos + link tem que ser separados por espaço, para adicionar musicas na playlist, é necessário reiniciar a mesma com o comando stop seguido de run ou random e os comandos skip, last e random não funcionam em playlists de terceiros, mas é possível deletar a playlist que já está tocando e criar uma nova sem que a música pare_
