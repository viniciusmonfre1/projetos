# -*- coding: utf-8 -*-

import logging
import random
#from google import search
from aiogram import Bot, Dispatcher, executor, types
from chatterbot.trainers import ListTrainer
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
import time


chatbot = ChatBot('MiniReminder')

treinar = ListTrainer(chatbot)
falas = open('falas.txt','r').readlines()
treinar.train(falas)





def run_bot(dp):
    @dp.message_handler()
    async def messages(message: types.Message):
        if 'jav' in message.text.lower():
            response, reply = get_random_response()
            await message.reply(response, reply=reply)
        
        elif 'linux' in message.text.lower():
            response, reply = get_random_response_linux()
            await message.reply(response, reply=reply)

        elif 'ubuntu' in message.text.lower():
            response, reply = get_random_response_ubuntu()
            await message.reply(response, reply=reply)

        elif 'arch' in message.text.lower():
            response, reply = get_random_response_arch()
            await message.reply(response, reply=reply)

        elif 'kuros' in message.text.lower():
            response, reply = get_random_response_kuros()
            await message.reply(response, reply=reply)
            
        elif 'codewalkers' in message.text.lower():
            response, reply = get_random_response_cw()
            await message.reply(response, reply=reply)
            
        else: 
           # response, reply = chatbot_responder(message.text)
           # await message.reply(response, reply=reply)
            
            #time.sleep( 5 )
            
            response, reply = chatbot_responder(message.text)
            await message.reply(response, reply=reply)


def get_random_response():
    possible_responses = [
        ("@Tardis96TheWalker, estão te chamando aí", 0), 
    ]
    return random.choice(possible_responses)


def get_random_response_linux():
    possible_responses = [
        ("Gostei dessa mensagem. @Tardis96TheWalker da uma olhada", 1),
    ]
    return random.choice(possible_responses)


def get_random_response_ubuntu():
    possible_responses = [
        ("Gostei dessa mensagem. @Tardis96TheWalker da uma olhada", 1),
    ]
    return random.choice(possible_responses)


def get_random_response_arch():
    possible_responses = [
        ("Gostei dessa mensagem. @Tardis96TheWalker da uma olhada", 1),
    ]
    return random.choice(possible_responses)



def get_random_response_kuros():
    possible_responses = [
        ("ele está em desenvolvimento", 1),
        ("o KurOS é um sistema operacional desenvolvido pela Iniciativa CodeWalkers,com o foco em auxiliar as pessoas que estão adentrando esse vasto mubndo que é a programação.", 0),
        ("o KurOS é um sistema Lindo. olha esse vídeo https://www.youtube.com/watch?v=cZUiZqduDc8&t=121s", 1),
        ("qualquer dúvida, fale com o @Tardis96TheWalker. ele é o mantenedor do sistema", 1),
        ("Inicialmente só havia o Klaus. ele queria aprender sobre programação e sobre tecnologia. então conheceu 2 amigos em grupos. Reimer e Eduardo. depois de um tempo e algumas tretas, os 3 decidiram criar um grupo próprio. e assim surgiu o CodeWalkers. um belo dia o grupo tinha umas 100 pessoas quando chegou um doido botando pilha em todo mundo e graças a esse doido, o Grupo Codewalkers decidiu começar a desenvolver um Sistema Operacional. esse sistema operacional é o KurOS e o doido é o Vinicius Monfre", 1),
        ("o KurOS é um sistema fácil de usar e muito completo", 0),
        ("Se interessou pelo Kuros ? veja os outros projetos da iniciativa CodeWalkers", 1),
        ("System.out.Print('olá KurOS')", 1),
        ("Estamos com uma equipe pequena ainda. mas quem sabe um dia, possamos ser grandes", 0),
        ("KurOS. Uma iniciativa CodeWalkers, Seja feliz sendo um eterno Aprendiz", 1)
    ]
    return random.choice(possible_responses)

def get_random_response_cw():
    possible_responses = [
        ("Seja Feliz sendo um eterno aprendiz", 1),
        ("Bora Abrender a programar gente", 0),
        ("Dá uma olhada no blog https://codewalkers.org/", 1),
        ("quero ver todo mundo programando heim", 1),
        ("esse grupo é incrível", 0),
        ("A programação é uma arte. e os CodeWalkers apreciam muito essa arte", 1)
    ]
    return random.choice(possible_responses)
    
    

def chatbot_responder(input):
        return (chatbot.get_response(input),0)
	

if __name__ == '__main__':
    API_TOKEN = 'Bot_Token_Here'
    logging.basicConfig(level=logging.INFO)
    bot = Bot(token=API_TOKEN)
    dp = Dispatcher(bot)
    run_bot(dp)
    executor.start_polling(dp, skip_updates=True)
