package com.mycompany.impress;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author vinicius
 */
import java.sql.*;

public class UsuarioDAO {
    // private final Connection connection;

    Connection connection;
    String supnome = null;
    String supsenha = null;
    String endereco = null;
    String printer = null;
    String id = null;

    public UsuarioDAO() {
        this.connection = new ConnectionFactory().getConnection();
    }
// esse método adiciona um cadastro no banco
    Usuario usuario = new Usuario();

    public void adiciona(Usuario usuario) throws SQLException {
        String sql = "INSERT INTO usuario( endereco, cadastro) VALUES(?,?)";
        //  String supsql = "INSERT INTO suporte( supnome, supsenha) VALUES(?,?)";
        try {
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {

                stmt.setString(1, usuario.getendereco());
                stmt.setString(2, usuario.getcadastro());
                stmt.execute();
                stmt.close();
            }
        } catch (SQLException u) {
            throw new RuntimeException(u);
        } finally {
            System.out.println("Closing a conection");
            connection.clearWarnings();
            System.out.println();
            connection.close();
            System.out.println("Connection closed.........");
        }

    }

    public void Impressora(Usuario usuario) throws SQLException {
        String sql = "UPDATE usuario set printer=? WHERE id = ?";
        //  String supsql = "INSERT INTO suporte( supnome, supsenha) VALUES(?,?)";
        try {
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {

                stmt.setString(1, usuario.getprinter());
                stmt.setString(2, usuario.id);
                stmt.execute();
                stmt.close();
                // connection.close();
                // connection = null;

            }

            //   try (PreparedStatement stmt = connection.prepareStatement(supsql)) {
            //      stmt.setString(1, usuario.getsuplogin());
            //      stmt.setString(2, usuario.getsupsenha());
            //     stmt.execute();
            //  }
        } catch (SQLException u) {
            throw new RuntimeException(u);
        } finally {
            System.out.println("Closing a conection");
            connection.clearWarnings();
            System.out.println();
            connection.close();
            System.out.println("Connection closed.........");
        }

    }

    //esse método edita um cadastro
    public void Edita(Usuario usuario) throws SQLException {
        String sql = "UPDATE usuario set endereco=?,cadastro=? WHERE id = ?";
        try {
            try (PreparedStatement pstm = connection.prepareStatement(sql)) {
                pstm.setString(1, usuario.endereco);
                pstm.setString(2, usuario.cadastro);
                pstm.setString(3, usuario.id);
                pstm.executeUpdate();
            }
            // connection.close();
            // connection = null;

        } catch (SQLException u) {
            throw new RuntimeException(u);
        } finally {
            System.out.println("Closing a conection");
            connection.clearWarnings();
            System.out.println();
            connection.close();
            System.out.println("Connection closed.........");
        }

    }

    //esse método faz a leitura dos dados do banco para as suas respectivas variáveis
    public void ler(Usuario usuario) throws SQLException {
        String query = "SELECT * FROM usuario";
        Statement st = connection.createStatement();
        try (ResultSet rs = st.executeQuery(query)) {
            while (rs.next()) {
                String end = rs.getString("endereco");
                String prin = rs.getString("printer");
                String Cad = rs.getString("cadastro");
                String ID = rs.getString("id");
                usuario.setendereco(end);
                usuario.setprinter(prin);
                usuario.setcadastro(Cad);
                usuario.setid(ID);
            }
        }

    }

    private String Scope() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

}
