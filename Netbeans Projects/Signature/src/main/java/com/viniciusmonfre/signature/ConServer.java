/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viniciusmonfre.signature;

import com.viniciusmonfre.signature.UsuarioDAO;
import java.io.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

/**
 *
 * @author vinicius
 */
public class ConServer {

    Usuario usuario = new Usuario();

    public void executa(Usuario usuario) throws IOException, SQLException {
        UsuarioDAO dao = new UsuarioDAO();
        dao.ler(usuario);
        String endereco = usuario.endereco;
        String impressora = usuario.printer;
        //String impressora = "{" +"\"ImpressoraTermicaGuid\": [" +"\"87008eba-10bc-41f2-9068-3fe298acbc36\"," +"\"00000000-0000-0000-0000-000000000000\"" +"]" +"}";

        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {

            HttpPost request = new HttpPost("http://" + endereco + "/rest/ProcWSImp");
            //request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");

            request.setEntity(new StringEntity(impressora));
            HttpResponse response = client.execute(request);

            BufferedReader bufReader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent()));

            StringBuilder builder = new StringBuilder();

            String line;

            while ((line = bufReader.readLine()) != null) {

                builder.append(line);
                builder.append(System.lineSeparator());
            }

            //JOptionPane.showMessageDialog(null,builder);
            //JOptionPane.showMessageDialog(null,body);
            String retorno = builder.toString();
            System.out.println(retorno);

            System.out.println(retorno);
            JSONObject obj = new JSONObject(retorno);

            String impresstxt = obj.getString("ImpressaoTexto");
            String impresscaminho = obj.getString("ImpressoraCaminho");

            System.out.println("texto " + impresstxt);
            System.out.println("caminho" + impresscaminho);
            System.out.println("id " + usuario.id);
            System.out.println(builder);
            // salva arquivo com o código da impressora
              JSONObject vai = new JSONObject(impressora);
              
           //  String texto = vai.getString("ImpressoraTermicaGuid");
            
            String texto = impressora;
                System.out.println(texto);
           // System.out.println(texto);
            //verifica se o retorno é válido
            //se for válido o programa continua a sua execução
            //se não for válido ele pula esse bloco
            if (!"".equals(impresstxt) && !"".equals(impresscaminho)) {
                //implementa a instancia para escrever o arquivo
                FileWriter arquivo;

                try {
                    // escreve o arquvo .txt

                    arquivo = new FileWriter(new File(texto + ".txt"));
                    arquivo.write(impresstxt);
                    arquivo.close();
                } catch (IOException e) {
                } catch (Exception e) {
                }

//copia o arquvo via comando
                Process p = Runtime.getRuntime().exec("cmd /c copy " + texto + ".txt" + " " + impresscaminho);
                System.out.println("cmd " + texto + ".txt" + " " + impresscaminho);
//apaga o arquivo.txt
                Process q = Runtime.getRuntime().exec("cmd /c del " + texto + ".txt");
            }

        }

    }

    public void roda(Usuario usuario) throws IOException, SQLException {
        UsuarioDAO dao = new UsuarioDAO();
        dao.ler(usuario);
        String endereco = usuario.endereco;
        String cadastro = usuario.cadastro;

        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {

            HttpPost request = new HttpPost("http://" + endereco + "/rest/ProcWSCadImp");
            //request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");

            try {
                request.setEntity(new StringEntity(cadastro));

                HttpResponse response = client.execute(request);

                BufferedReader bufReader = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));

                StringBuilder builder = new StringBuilder();

                String line;

                while ((line = bufReader.readLine()) != null) {

                    builder.append(line);
                    builder.append(System.lineSeparator());
                }

                //JOptionPane.showMessageDialog(null,builder);
                //JOptionPane.showMessageDialog(null,body);
                String retorno = builder.toString();
                usuario.printer = retorno;
                dao.Impressora(usuario);

                //System.out.println(retorno);
            } catch (IOException ex) {
                Logger.getLogger(ConServer.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
