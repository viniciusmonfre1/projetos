/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viniciusmonfre.signature;

/**
 *
 * @author vinicius
 */ 
  
import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.util.*;
import javax.swing.JOptionPane;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.*;
import javax.xml.crypto.dsig.spec.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;  
import org.w3c.dom.NodeList;  
  
/** 
 
@author Roberto 
*/  
public class OnCert {  
public static PrivateKey chavinha;
public static PublicKey chavona;
 public static String file = null;
 public static String alias = null;
    //Procedimento que retorna o Keystore  
  

    /**
     *
     * @param strAliasTokenCert
     * @return
     * @throws NoSuchProviderException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     * @throws UnrecoverableEntryException
     */
    public static KeyStore funcKeyStore(String strAliasTokenCert) throws NoSuchProviderException,  
            IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException {  
  
        String strResult = "";  
        KeyStore ks = null;  
  
        try {  
            ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");  
            ks.load(null, null);  
  
            Enumeration<String> aliasEnum = ks.aliases();  
  
            while (aliasEnum.hasMoreElements()) {  
                String aliasKey = (String) aliasEnum.nextElement();  
  
                if (ks.isKeyEntry(aliasKey)) {  
                    strResult = aliasKey;  
                }  
  
                if (ks.getCertificateAlias(ks.getCertificate(strResult)) == null ? strAliasTokenCert == null : ks.getCertificateAlias(ks.getCertificate(strResult)).equals(strAliasTokenCert)) {  
                    break;  
                }  
            }  
  
        } catch (KeyStoreException ex) {  
            System.out.println("ERROR " + ex.getMessage());  
        }  
  
        return ks;  
  
    }  
  
    //Procedimento de listagem dos certificados digitais  

    /**
     *
     * @param booCertValido
     * @return
     * @throws NoSuchProviderException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     */
    public static String[] funcListaCertificados(boolean booCertValido) throws NoSuchProviderException,  
            IOException, NoSuchAlgorithmException, CertificateException {  
  
        //Estou setando a variavel para 20 dispositivos no maximo  
        String strResult[] = new String[20];  
        Integer intCnt = 0;  
  
        try {  
            KeyStore ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");  
            ks.load(null, null);  
  
            Enumeration<String> aliasEnum = ks.aliases();  
  
            while (aliasEnum.hasMoreElements()) {  
                String aliasKey = (String) aliasEnum.nextElement();  
  
                if (booCertValido == false) {  
                    strResult[intCnt] = aliasKey;  
                } else if (ks.isKeyEntry(aliasKey)) {  
                    strResult[intCnt] = aliasKey;  
                }  
  
                if (strResult[intCnt] != null) {  
                    intCnt = intCnt + 1;  
  
                }  
  
            }  
  
        } catch (KeyStoreException ex) {  
            System.out.println("ERROR " + ex.getMessage());  
        }  
  
        return strResult;  
  
    }  
  
    //Procedimento que retorna a chave privada de um certificado Digital  

    /**
     *
     * @param strAliasTokenCert
     * @param strAliasCertificado
     * @param strArquivoCertificado
     * @param strSenhaCertificado
     * @return
     * @throws Exception
     */
    public static PrivateKey funcChavePrivada(String strAliasTokenCert, String strAliasCertificado,  
            String strArquivoCertificado, String strSenhaCertificado) throws Exception {  
  
        KeyStore ks = null;  
        PrivateKey privateKey = null;  
  
        if (strAliasTokenCert == null || "".equals(strAliasTokenCert)) {  
  
            ks = KeyStore.getInstance("PKCS12");  
            FileInputStream fis = new FileInputStream(strArquivoCertificado);  
            //Efetua o load do keystore  
            ks.load(fis, strSenhaCertificado.toCharArray());  
            //captura a chave privada para a assinatura  
            privateKey = (PrivateKey) ks.getKey(strAliasCertificado, strSenhaCertificado.toCharArray());  
  
        } else {  
  
            if (strSenhaCertificado == null || "".equals(strSenhaCertificado)) {  
                strSenhaCertificado = "Senha";  
            }  
  
            //Procedimento para a captura da chave privada do token/cert  
            privateKey = (PrivateKey) funcKeyStore(strAliasTokenCert).getKey(strAliasTokenCert, strSenhaCertificado.toCharArray());  
  
        }  
  
        //JOptionPane.showMessageDialog(null,privateKey.toString());  
        chavinha = privateKey;
        AssinaturaDigital.chavinha = chavinha;
        return privateKey;  
  
    }  
  
    //Procedimento que retorna a chave publica de um certificado Digital  

    /**
     *
     * @param strAliasTokenCert
     * @param strAliasCertificado
     * @param strArquivoCertificado
     * @param strSenhaCertificado
     * @return
     * @throws Exception
     */
    public static PublicKey funcChavePublica(String strAliasTokenCert, String strAliasCertificado, String strArquivoCertificado, String strSenhaCertificado) throws Exception {  
  
        KeyStore ks = null;  
        PublicKey chavePublica = null;  
  
        if (strAliasTokenCert == null || "".equals(strAliasTokenCert)) {  
  
            ks = KeyStore.getInstance("PKCS12");  
            //InputStream entrada para o arquivo
            try (FileInputStream fis = new FileInputStream(strArquivoCertificado)) {
                //InputStream entrada para o arquivo
                ks.load(fis, strSenhaCertificado.toCharArray());
            }  
            Key chave = (Key) ks.getKey(strAliasCertificado, strSenhaCertificado.toCharArray());  
            //O tipo de dado é declarado desse modo por haver ambigüidade (Classes assinadas com o mesmo nome "Certificate")  
            java.security.cert.Certificate cert = (java.security.cert.Certificate) ks.getCertificate(strAliasCertificado);  
            chavePublica = cert.getPublicKey();  
  
        } else {  
  
            if (strSenhaCertificado == null || "".equals(strSenhaCertificado)) {  
                strSenhaCertificado = "Senha";  
            }  
  
            //Procedimento se for utilizar token para a captura de chave publica  
            ks = funcKeyStore(strAliasTokenCert);  
            Key key = ks.getKey(strAliasTokenCert, strSenhaCertificado.toCharArray());  
            java.security.cert.Certificate crtCert = ks.getCertificate(strAliasTokenCert); 
            if(key == null){
                JOptionPane.showMessageDialog(null, "Erro na leitura do certificado", "Erro Crítico", JOptionPane.ERROR_MESSAGE);
            }
            chavePublica = crtCert.getPublicKey();  
  
        }  
        chavona = chavePublica;
        AssinaturaDigital.chavona = chavona;
        return chavePublica;  
  
    }  
  
    //Procedimento que verifica a assinatura  

    /**
     *
     * @param pbKey
     * @param bteBuffer
     * @param bteAssinado
     * @param strAlgorithmAssinatura
     * @return
     * @throws Exception
     */
    public static boolean funcAssinaturaValida(PublicKey pbKey, byte[] bteBuffer, byte[] bteAssinado, String strAlgorithmAssinatura) throws Exception {  
  
        if (strAlgorithmAssinatura == null) {  
            strAlgorithmAssinatura = "MD5withRSA";  
        }  
  
        Signature isdAssinatura = Signature.getInstance(strAlgorithmAssinatura);  
        isdAssinatura.initVerify(pbKey);  
        isdAssinatura.update(bteBuffer, 0, bteBuffer.length);  
        return isdAssinatura.verify(bteAssinado);  
  
    }  
  
    //Procedimento que gera a assinatura  

    /**
     *
     * @param pbKey
     * @param bteBuffer
     * @param strAlgorithmAssinatura
     * @return
     * @throws Exception
     */
    public static byte[] funcGeraAssinatura(PrivateKey pbKey, byte[] bteBuffer, String strAlgorithmAssinatura) throws Exception {  
  
       
            strAlgorithmAssinatura = "MD5withRSA";  
        
  
        Signature isdAssinatura = Signature.getInstance(strAlgorithmAssinatura);  
        isdAssinatura.initSign(pbKey);  
        isdAssinatura.update(bteBuffer, 0, bteBuffer.length);  
        return isdAssinatura.sign();  
  
    }  
  
    //Procedimento que retorna o status do certificado  

    /**
     *
     * @param crtCertificado
     * @return
     */
    public static String funcStatusCertificado(X509Certificate crtCertificado) {  
  
        try {  
            crtCertificado.checkValidity();  
            return "Certificado válido até"+crtCertificado.getNotAfter();  
        } catch (CertificateExpiredException E) {  
            return "Certificado expirado!";  
        } catch (CertificateNotYetValidException E) {  
            return "Certificado inválido!";  
        }  
  
    }  
  
    //Procedimento que retorna o certificado selecionado  

    /**
     *
     * @param strAliasTokenCert
     * @param strAliasCertificado
     * @param strSenhaCertificado
     * @return
     * @throws NoSuchProviderException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     * @throws UnrecoverableEntryException
     * @throws KeyStoreException
     */
    public static X509Certificate funcCertificadoSelecionado(String strAliasTokenCert, String strAliasCertificado, String strSenhaCertificado) throws NoSuchProviderException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException, KeyStoreException {  
  
        X509Certificate crtCertificado = null;  
        KeyStore crtRepositorio = null;  
  
        if (strAliasTokenCert == null || "".equals(strAliasTokenCert)) {  
            //a principio esta desativado  
            //Procedimento de captura do certificao arquivo passado como parametro  
            //InputStream dado = new FileInputStream(strArquivoCertificado);  
            //crtRepositorio = KeyStore.getInstance("PKCS12");  
            //crtRepositorio.load(dado, strSenhaCertificado.toCharArray());  
            //crtCertificado = (X509Certificate) crtRepositorio.getCertificate(strAliasCertificado);  
  
        } else {  
  
            if (strSenhaCertificado == null || "".equals(strSenhaCertificado)) {  
                strSenhaCertificado = "nao passou senha";  
            }  
  
            //Procedimento de captura do certificao token passado como parametro  
            KeyStore.PrivateKeyEntry keyEntry;  
            try {  
  
                keyEntry = (KeyStore.PrivateKeyEntry) funcKeyStore(strAliasTokenCert).getEntry(strAliasTokenCert, new KeyStore.PasswordProtection(strSenhaCertificado.toCharArray()));  
  
                crtCertificado = (X509Certificate) keyEntry.getCertificate();  
            } catch (KeyStoreException ex) {  
            }  
        }  
  
  
        return crtCertificado;  
  
    }  
  
    /**
     *
     */
    public static class TAssinaXML {  
        
        
        
        //MD2withRSA - MD5withRSA - SHA1withRSA - SHA224withRSA - SHA256withRSA - SHA1withDSA - DSA - RawDSA  
        public static  String strAlgorithmAssinatura = "SHA256withRSA";  

        /**
         *
         */
        public static String strAliasTokenCert = AssinaturaDigital.alias;  

        /**
         *
         */
        public static String strAliasCertificado = AssinaturaDigital.alias;  

        /**
         *
         */
        public static String strArquivoCertificado = null;  

        /**
         *
         */
        public static String strSenhaCertificado = null;  

        /**
         *
         */
        public static  String strArquivoXML = file;  

        /**
         *
         */
        public static  String strArquivoSaveXML = file+"assinado.xml";  

        /**
         *
         */
        public static String C14N_TRANSFORM_METHOD = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";  

        /**
         *
         */
        public boolean booNFS = true;  
    }  
    //Procedimento de assinar XML  
  
    /**
     *
     * @param tpAssinaXML
     * @param operacao
     * @return
     * @throws Exception
     */
    //TAssinaXML tpAssinaXML,
    public static boolean funcAssinaXML( String operacao) throws Exception {  
  
  
        /* 
        operacao 
        '1' - NFE 
        '2' - CANCELAMENTO 
        '3' - INUTILIZACAO 
         */  
        String tag = "1";  
        switch (operacao) {
            case "1":
                tag = "infNFe";
                break;
            case "2":
                tag = "infCanc";
                break;  
            case "3":
                tag = "infInut";
                break;
            default:
                break;
        }
  
  
  
        XMLSignatureFactory sig = null;  
        SignedInfo si = null;  
        KeyInfo ki = null;  
        String strTipoSign = tag;  
        String strID = "Id";  
  
          
        //Capturo o certificado  
        X509Certificate cert = funcCertificadoSelecionado(TAssinaXML.strAliasTokenCert, TAssinaXML.strAliasCertificado, TAssinaXML.strSenhaCertificado);  
  
        //Inicializo o arquivo/carrego  
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
        dbf.setNamespaceAware(true);  
        Document doc = dbf.newDocumentBuilder().parse(new FileInputStream(TAssinaXML.strArquivoXML));  
  
        sig = XMLSignatureFactory.getInstance("DOM");  
  
        ArrayList<Transform> transformList = new ArrayList<>();  
        Transform enveloped = sig.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null);  
        Transform c14n = sig.newTransform(TAssinaXML.C14N_TRANSFORM_METHOD, (TransformParameterSpec) null);  
        transformList.add(enveloped);  
        transformList.add(c14n);  
  
        NodeList elements = doc.getElementsByTagName(strTipoSign);  
        org.w3c.dom.Element el = (org.w3c.dom.Element) elements.item(0);  
  
        String id = el.getAttribute(strID);  
  
        Reference r = sig.newReference("#".concat(id), sig.newDigestMethod(DigestMethod.SHA1, null),  
                transformList,  
                null, null);  
        si = sig.newSignedInfo(  
                sig.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE,  
                (C14NMethodParameterSpec) null),  
                sig.newSignatureMethod(SignatureMethod.RSA_SHA1, null),  
                Collections.singletonList(r));  
  
        KeyInfoFactory kif = sig.getKeyInfoFactory();  
        List x509Content = new ArrayList();  
        x509Content.add(cert);  
        X509Data xd = kif.newX509Data(x509Content);  
        ki = kif.newKeyInfo(Collections.singletonList(xd));  
  
        DOMSignContext dsc = new DOMSignContext(funcChavePrivada(TAssinaXML.strAliasTokenCert, TAssinaXML.strAliasCertificado, TAssinaXML.strArquivoCertificado, TAssinaXML.strSenhaCertificado), doc.getDocumentElement());  
        XMLSignature signature = sig.newXMLSignature(si, ki);  
  
        signature.sign(dsc);  
  
        //Salvo o arquivo assinado  
        OutputStream os = new FileOutputStream(TAssinaXML.strArquivoSaveXML);  
        TransformerFactory tf = TransformerFactory.newInstance();  
        Transformer trans = tf.newTransformer();  
        trans.transform(new DOMSource(doc), new StreamResult(os));  
  
        return true;  
  
    }  
} 
