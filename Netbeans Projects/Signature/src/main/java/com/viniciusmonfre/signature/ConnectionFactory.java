package com.viniciusmonfre.signature;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author vinicius
 */
/* Obtém o driver de conexão com o banco de dados */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {

    //instancia driver jdbc e faz a conexão com o banco impressora
    private static final String URL = "jdbc:sqlite:impressora";

    /* Tenta se conectar */
    public Connection getConnection() {

        try {
            /* Caso a conexão ocorra com sucesso, a conexão é retornada */
            Connection con = DriverManager.getConnection(URL);
            return con;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Connection con = DriverManager.getConnection(URL);
            return con;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
}
