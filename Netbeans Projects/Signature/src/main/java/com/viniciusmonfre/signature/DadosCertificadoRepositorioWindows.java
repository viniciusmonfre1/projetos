/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viniciusmonfre.signature;
  
import static com.viniciusmonfre.signature.OnCert.funcKeyStore;
import java.io.FileInputStream;
import java.io.IOException;
import static java.lang.System.out;
import java.security.Key;
import java.security.KeyStore;  
import static java.security.KeyStore.getInstance;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;  
import java.util.Enumeration;  
import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector.Purpose;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.X509Data;
  
/** 
* Acessa dados dos Certificados Digitais por meio do repositorio do Windows (SunMSCAPI). 
*  
* @author Copyright (c) 2012 Maciel Gonçalves 
*  
* Este programa é software livre, você pode redistribuí-lo e ou modificá-lo 
* sob os termos da Licença Pública Geral GNU como publicada pela Free 
* Software Foundation, tanto a versão 2 da Licença, ou (a seu critério) 
* qualquer versão posterior. 
*  
* http://www.gnu.org/licenses/gpl.txt 
*  
*/  
public class DadosCertificadoRepositorioWindows {  
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
      
    /**
     *
     */
   public static PrivateKey chavinha;
   public static PublicKey chavona;
    public static void CertWin() {  
        try {  
            KeyStore keyStore = getInstance("Windows-MY", "SunMSCAPI");  
            keyStore.load(null, null);  
              
            Enumeration <String> al = keyStore.aliases();  
            while (al.hasMoreElements()) {  
                String alias = al.nextElement();  
                info("--------------------------------------------------------");  
                if (keyStore.containsAlias(alias)) {  
                    info("Emitido para........: " + alias);  
  
                    X509Certificate cert = (X509Certificate) keyStore.getCertificate(alias);  
                    info("SubjectDN...........: " + cert.getSubjectDN().toString());  
                    info("Version.............: " + cert.getVersion());  
                    info("SerialNumber........: " + cert.getSerialNumber());  
                    info("SigAlgName..........: " + cert.getSigAlgName());  
                    info("Válido a partir de..: " + dateFormat.format(cert.getNotBefore()));  
                    info("Válido até..........: " + dateFormat.format(cert.getNotAfter()));    
                } else {  
                    info("Alias doesn't exists : " + alias);  
                }  
            }  
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException | CertificateException e) {  
            error(e.toString());  
        }  
    }
    
    public static PrivateKey funcChavePrivada(String strAliasTokenCert, String strAliasCertificado,  
            String strArquivoCertificado, String strSenhaCertificado) throws Exception {  
  
        KeyStore ks = null;  
        PrivateKey privateKey = null;  
  
        if (strAliasTokenCert == null || "".equals(strAliasTokenCert)) {  
  
            ks = KeyStore.getInstance("PKCS12");  
            FileInputStream fis = new FileInputStream(strArquivoCertificado);  
            //Efetua o load do keystore  
            ks.load(fis, strSenhaCertificado.toCharArray());  
            //captura a chave privada para a assinatura  
            privateKey = (PrivateKey) ks.getKey(strAliasCertificado, strSenhaCertificado.toCharArray());  
  
        } else {  
  
            if (strSenhaCertificado == null || "".equals(strSenhaCertificado)) {  
                strSenhaCertificado = "Senha";  
            }  
  
            //Procedimento para a captura da chave privada do token/cert  
            privateKey = (PrivateKey) funcKeyStore(strAliasTokenCert).getKey(strAliasTokenCert, strSenhaCertificado.toCharArray());  
  
        }  
  
        //JOptionPane.showMessageDialog(null,privateKey.toString());  
        
         chavinha = privateKey;
        return privateKey;  
  
    }  
    
    
 
  
    /** 
     * Info. 
     * @param log 
     */  
    private static void info(String log) {  
        out.println("INFO: " + log);  
    }  
  
    /** 
     * Error. 
     * @param log 
     */  
    private static void error(String log) {  
        out.println("ERROR: " + log);  
    }  

    class Funselect {

       public KeySelectorResult Funselect(KeyInfo keyInfo, Purpose purpose, AlgorithmMethod method, XMLCryptoContext context)
		throws KeySelectorException {

	for (Object o : keyInfo.getContent()) {
		if (o instanceof X509Data) {
			for (Object o2 : ((X509Data) o).getContent()) {
				if (o2 instanceof X509Certificate) {
					final X509Certificate cert = (X509Certificate) o2;
					return () -> {
                                            chavona = cert.getPublicKey();
                                            return cert.getPublicKey();
                                        };
				}
			}
		}
	}

	return null;
}
    }
  
}  
