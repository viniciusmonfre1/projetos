from flask import Flask, request
import subprocess
import os
import signal
import requests
import datetime
app = Flask(__name__)
p = None
now = datetime.datetime.now()
@app.route('/inicialeitura', methods=['POST'])
def inicialeitura():
    global p
    print(now)
    data = request.get_json()

    #print("DATA " + str(data))
    idEquipamento = data['idEquipamento']
    ipServerCEPP = data['ipServerCEPP']
    nomeAplicacao = data['nomeAplicacao']
    objetoRequisicaoRest = data['objetoRequisicaoRest']
    portaServidor = data['portaServidor']
    protocoloComunicacao = data['protocoloComunicacao']
    arquivo = open('/home/orangepi/ceppPy/logs/idEquipamentow.cepp', 'w')
    arquivo.write(str(idEquipamento))
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/ipServerCEPPw.cepp', 'w')
    arquivo.write(ipServerCEPP)
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/nomeAplicacaow.cepp', 'w')
    arquivo.write(nomeAplicacao)
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/objetoRequisicaoRestw.cepp', 'w')
    arquivo.write(objetoRequisicaoRest)
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/portaServidorw.cepp', 'w')
    arquivo.write(portaServidor)
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/protocoloComunicacaow.cepp', 'w')
    arquivo.write(protocoloComunicacao)
    arquivo.close()
    with open('/home/orangepi/ceppPy/logs/lock.cepp', 'r') as arquivo:
        lock = arquivo.read()
    if lock == "Lock":
        print(now, " a trava já existe")
    else:
         p = subprocess.Popen(["python3","/home/orangepi/ceppPy/Compilado/CeppCam.py"], stdout = subprocess.PIPE)
    arquivo = open('/home/orangepi/ceppPy/logs/lock.cepp', 'w')
    arquivo.write("Lock")
    arquivo.close()
    
    print(data)
    return 'leitura Iniciada'

@app.route('/encerraleitura')
def encerraleitura():
    print(now)
    arquivo = open('/home/orangepi/ceppPy/logs/idEquipamentow.cepp', 'w')
    arquivo.write("")
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/ipServerCEPPw.cepp', 'w')
    arquivo.write("")
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/nomeAplicacaow.cepp', 'w')
    arquivo.write("")
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/objetoRequisicaoRestw.cepp', 'w')
    arquivo.write("")
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/portaServidorw.cepp', 'w')
    arquivo.write("")
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/protocoloComunicacaow.cepp', 'w')
    arquivo.write("")
    arquivo.close()
    arquivo = open('/home/orangepi/ceppPy/logs/lock.cepp', 'w')
    arquivo.write("")
    arquivo.close()
    p.kill()
    return 'leitura Encerrada'

@app.route('/')
def server():
    print(now)
    return 'OK'

@app.route('/acendeled', methods=['POST'])
def acendeled():
    dados = request.get_json()
    obj = str(dados).replace("'", '"')
    obj = obj.replace(" ", "")
    print(obj)
    x = requests.post("http://localhost:3334/acendeled", data = obj, headers = "Content-type: application/json")
    print(x.text)
    return 'Led Aceso'
@app.route('/apagaled', methods=['POST'])
def apagaled():
    dados = request.get_json()
    obj = str(dados).replace("'", '"')
    obj = obj.replace(" ", "")
    print(obj)
    led = data['led']
    x = requests.post("http://localhost:3334/apagaled", data = obj, headers = {"Content-type": "application/json"})
    print(x.text)
    return 'Led Apagado'
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug='false', port = 3333)
