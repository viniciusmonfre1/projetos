# -*- coding: UTF-8 -*-
import base64
import time
import sh
import re
import io
from io import BytesIO
import pathlib  
import logging
import random
import asyncio
import logging
import aiogram.utils.markdown as md
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import ParseMode
from aiogram.utils import executor
from aiogram.types import ContentTypes, Message
import requests
import os
import concurrent.futures

logging.basicConfig(level=logging.INFO)

#API_TOKEN = '1320365968:AAHAeNeke-Oom8FlkQ1MFn0dALRkwO5tx84'
API_TOKEN = '5227225712:AAGYp9k8sg_vZRIWF5WxspiaxDmwjnw3KWg'

bot = Bot(token=API_TOKEN)

# For example use simple MemoryStorage for Dispatcher.
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
regex = '([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+'
mot = 0
json_dump = ""
src = ""
link = "http://localhost:8941/suporteTEGA/rest/"
#link = "http://192.168.100.80:8080/suporte17Java/rest/"
msgsoft = ""
msgmod = ""
mime = []
encoded = ""
doc = "/srv/apps/telegrambot/file"
#doc = "file"
encodedimage = ""
mimeimage = []   
img64 = []
arq64 = []
imagem = []
arquivo = []
quebra = 0
attach = ""
last = 0
# States
class Form(StatesGroup):
    nome = State()  #Will be represented in storage as 'Form:nome'
    empresa = State()
    email = State()
    fone = State()
    motivo = State()
    desc = State() # prio = State()
    software = State()
    modulo = State()
    escolha = State()
    arq = State()
    escfoto = State()
    foto = State()
    escvideo = State()
    video = State()
    obs = State()
    confirm = State()
@dp.message_handler(commands='start')
async def cmd_start(message: types.Message):
    """
    Conversation's entry point
    """
    # Set state
    await Form.nome.set()

    await types.ChatActions.upload_photo()
    await asyncio.sleep(1)
    media = types.MediaGroup()
    media.attach_photo('https://media.discordapp.net/attachments/927892935415050292/946464631130558494/assistente_robo_-_Tegabot.png', 'Oi, eu me chamo TegaBot e estarei realizando seu atendimento hoje! \n \n Para iniciar o chamado, por favor insira seu nome de usuário.')
    await message.reply_media_group(media=media)
    print("start")


# You can use state '*' if you need to handle all states
@dp.message_handler(state='*', commands='cancel')
@dp.message_handler(Text(equals='cancel', ignore_case=True), state='*')
async def cancel_handler(message: types.Message, state: FSMContext):
    """
    Allow user to cancel any action
    """
    current_state = await state.get_state()
    if current_state is None:
        return

    logging.info('Cancelling state %r', current_state)
    # Cancel state and inform user about it
    await state.finish()
    # And remove keyboard (just in case)
    await message.reply('Cancelled.', reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Form.nome)
async def process_name(message: types.Message, state: FSMContext):
    """
    Process user name
    """
    async with state.proxy() as data:
        data['nome'] = message.text
    
    await Form.next()
    await types.ChatActions.typing()
    await asyncio.sleep(2)
    frase = str(data['nome']).split()
    possible_responses = [
        ("Perfeito " + frase[0] +", agora preciso do nome da empresa."),
        ("Opa " + frase[0] + ", Manda o nome da empresa pra mim."),
        ("Ótimo " + frase[0] + "! Me fala qual o nome da empresa pra eu poder verificar aqui."),
        ("Beleza " + frase[0] + ", fala pra mim o nome da empresa por favor."),
        (frase[0] + ", Me manda o nome da empresa por favor.")
    ]
    print("nome")
    resposta =  random.choice(possible_responses)
    await message.reply(resposta)   
@dp.message_handler(state=Form.empresa)
async def process_empresa(message: types.Message, state: FSMContext):
    """
    Processa a empresa
    """
    global msgsoft
    global userid
    global clientid
    print("empresa")
    async with state.proxy() as data:
        data['empresa'] = message.text
    nome = data['nome']
    empresa = data['empresa']
    data_set = {"UsuarioNome":nome,"NomeEmpresa":empresa}
    url = link + 'postValidaUsuarioBot'
    myobj = data_set
    header = {'Content-type': 'application/json'}
    x = requests.post(url, headers=header, json = myobj, timeout=2)
    formater = x.text
    arquivo = open('log.tegabot', 'w')
    arquivo.write(formater)
    arquivo.close()
    dados = formater.replace("{", "")
    dados = dados.replace("}", "")
    dados = dados.replace(":", " ")
    dados = dados.replace('"ClienteId"', "")
    dados = dados.replace('"UsuarioId"', "")
    dados = dados.split()
    num = dados[0]
    num = num[1:]
    num = num[:-1]
    num = num[:-1]
    clientid = num
    num = dados [1]
    num = num[1:]
    num = num[:-1]
    userid = num
    validator = "0"
    if userid == validator:
        if clientid == validator:
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("/start")
            await types.ChatActions.typing()
            await asyncio.sleep(1)
            await message.reply("Houve um erro na identificação da empresa, para tentar novamente clique no botão abaixo ou me envie o comando /start", reply_markup=markup)
            await state.finish()
        else:
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("/start")
            await types.ChatActions.typing()
            await asyncio.sleep(1)
            await message.reply("Houve um erro na identificação de usuário, para tentar novamente clique no botão abaixo ou me envie o comando /start", reply_markup=markup)
            await state.finish()
    else:
        url = link + "dpSoftwareCliente?Clienteid="+clientid
        x = requests.get(url, timeout=2)
        formater = x.text
        #print (formater)
        arquivo = open('log.tegabot', 'w')
        arquivo.write(formater)
        arquivo.close()
        formater = formater + " :"
        dados = formater.replace("{", "")
        dados = formater.replace("[", "")
        dados = formater.replace("]", "")
        dados = formater.replace(",", "")
        dados = dados.replace("SoftwareId", "")
        dados = dados.replace("SoftwareNome", "")
        #dados = dados.replace("}", "")
        dados = dados.replace(":", " ")
        dados = dados.replace('"ClienteId"', "")
        dados = dados.replace('"UsuarioId"', "")
        dados = dados.replace('"', '')
        dados = dados.replace("[", "")
        dados = dados.replace(")", "")
        dados = dados.replace("(", "")
        dados = dados.replace(")]", "")
        dados = dados.replace(")", "")
        dados = dados.replace("]", "")
        dados = dados.replace("{", "")
        dados = dados.split('}')
        msg = "Por Favor selecione O Software Abaixo: \n \n \n"
        index = ""
        infsoft = ""
        i = 0
        for number in range(len(dados)):
            num = dados[i]
            msg = msg + "/" + num[1:] + "\n"
            i = i + 1
        msgsoft = msg[:-3]
        await Form.next()
        await types.ChatActions.typing()
        await asyncio.sleep(2)
        possible_responses = [
            ("Perfeito, agora preciso do email de contato."),
            ("Opa, Manda o email de contato pra mim."),
            ("Ótimo! Me fala qual o email de contato."),
            ("Beleza, fala pra mim o email por favor."),
            ( " Me manda o email por favor.")
        ]
        resposta =  random.choice(possible_responses)
        await message.reply(resposta)   
@dp.message_handler(state=Form.email)
async def process_email(message: types.Message, state: FSMContext):
    """
    Processa o email
    """
    async with state.proxy() as data:
        print("email")
        data['email'] = message.text
        if(re.search(regex, data['email'])): 
            await types.ChatActions.typing()
            await asyncio.sleep(2)
            possible_responses = [
                 ("Perfeito, agora preciso do número de Telefone com DDD."),
                 ("Opa, Manda o Telefone com DDD pra mim."),
                 ("Ótimo! Me fala qual o DDD e o Telefone de contato."),
                 ("Beleza, fala pra mim o Telefone com DDD por favor."),
                 ( "Me manda o número de Telefone com o DDD por favor.")
            ]
            resposta =  random.choice(possible_responses)
            await message.reply(resposta)    
            await Form.next()  
                  
        else:  
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("/start")
            await types.ChatActions.typing()
            await asyncio.sleep(1)
            await message.reply("Houve um erro Durante a validação do email, para tentar novamente clique no botão abaixo ou me envie o comando /start", reply_markup=markup)
            await state.finish()
@dp.message_handler(state=Form.fone)
async def process_fone(message: types.Message, state: FSMContext):
    """
    Processa o fone
    """
    refone= "\(?(?:[14689][1-9]|2[12478]|3[1234578]|5[1345]|7[134579])\)? ?(?:[2-8]|9[1-9])[0-9]{3}\-?[0-9]{4}"
    print("fone")
    async with state.proxy() as data:
        data['fone'] = message.text
        formater = data['fone']
        arquivo = open('log.tegabot', 'w')
        arquivo.write(formater)
        arquivo.close()
    if(re.search(refone, data['fone'])):
        # Configure ReplyKeyboardMarkup
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("1-Dúvida/Erro de usuário", "2-Solicitação de especifíco (Módulo novo)", "3-Solicitação de melhoria")
        markup.add("4-Erro de sistema", "5-Instalação/Atualização", "6-Atendimento")
        await types.ChatActions.typing()
        await asyncio.sleep(2)
        frase = str(data['nome']).split()
        possible_responses = [
            ("Perfeito " + frase[0] +", Agora preciso saber o motivo do chamado. Selecione o motivo na lista:"),
            ("Opa " + frase[0] + ", Selecione o motivo do contato abaixo:"),
            ("Ótimo " + frase[0] + "! Seleciona qual o Motivo pra eu poder verificar aqui:"),
            ("Beleza " + frase[0] + ", clica pra mim no Motivo em um dos botões abaixo por favor:"),
            (frase[0] + ", selecione o motivo por favor:")
        ]
   
        resposta =  random.choice(possible_responses)
        await message.reply(resposta, reply_markup=markup)   
        await Form.next()  
                      
    else:  
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("/start")
        await types.ChatActions.typing()
        await asyncio.sleep(1)
        await message.reply("Houve um erro Durante a validação do Número de telefone, para tentar novamente clique no botão abaixo ou me envie o comando /start", reply_markup=markup)
        await state.finish()
        
  
@dp.message_handler(state=Form.motivo)
async def process_motivo(message: types.Message, state: FSMContext):
    """
    Processa o motivo
    """
    async with state.proxy() as data:
        print("motivo")
        it = str(message.text).split("-")
        data['motivo'] = it[0]
        types.ReplyKeyboardRemove()
    await Form.next()
    await types.ChatActions.typing()
    await asyncio.sleep(2)
    possible_responses = [
        ("Entendo, agora eu preciso de uma descrição resumida do seu problema."),
        ("Ah sim, descreva resumidamente o problema pra mim."),
        ("Compreendi, me manda uma breve descrição do que está acontecendo."),
        ("Entendi, descreva o ocorrido em poucas palavras por favor."),
        ("Compreendo, por favor me envie uma descrição curta do ocorrido.")
    ]
   
    resposta =  random.choice(possible_responses)
    await message.reply(resposta)
@dp.message_handler(state=Form.desc)
async def process_desc (message: types.Message, state: FSMContext):
    """
    Processa a descrição
    """
    global src
    global msgsoft
    async with state.proxy() as data:
        data['desc'] = message.text.upper()
        data['prio'] = "MEDIA"
        print("descricao")

    await Form.next()
    await types.ChatActions.typing()
    await asyncio.sleep(2)
   
    resposta =  msgsoft
    #print(resposta)
    await message.reply(resposta)
@dp.message_handler(state=Form.software)
async def process_software (message: types.Message, state: FSMContext):
    """
    Processa a descrição
    """
    global src
    global msgmod
    async with state.proxy() as data:
        data['software'] = message.text[1:]
    sof = data['software'] 
    print("software")
    url = link + "dpModulosSoftware?Softwareid="+sof
    x = requests.get(url, timeout=2)
    formater = x.text
    formater = formater + " :"
    dados = formater.replace("{", "")
    dados = formater.replace("[", "")
    dados = formater.replace("]", "")
    dados = formater.replace(",", " ")
    dados = dados.replace("ModuloNome", "")
    dados = dados.replace("ModuloId", "")
    dados = dados.replace(":", " ")
    dados = dados.replace('"ClienteId"', "")
    dados = dados.replace('"UsuarioId"', "")
    dados = dados.replace('"', '')
    dados = dados.replace("[", "")
    dados = dados.replace(")", "")
    dados = dados.replace("(", "")
    dados = dados.replace(")]", "")
    dados = dados.replace(")", "")
    dados = dados.replace("]", "")
    dados = dados.replace("{", "")
    dados = dados.replace("\r\n", "")
    dados = dados.replace("' ", "")
    dados = "-" + dados
    dados = dados.split('}')
    
    msg = "Por Favor selecione O módulo Abaixo: \n \n \n"
    index = ""
    infsoft = ""
    i = 0
    for number in range(len(dados)):
        num = dados[i]
        msg = msg + "/" + num[2:] + "\n"
        i = i + 1
    msgmod = msg[:-3]
    await Form.next()
    await types.ChatActions.typing()
    await asyncio.sleep(2)
    resposta =  msgmod
    await message.reply(resposta)
@dp.message_handler(state=Form.modulo)
async def process_modulo (message: types.Message, state: FSMContext):
    """
    Processa o módulo
    """
    global src
    async with state.proxy() as data:
        data['modulo'] = message.text[1:]
    mod = data['modulo']
    print("modulo") 
    await Form.next()

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add("SIM", "NÃO")
    await types.ChatActions.typing()
    await asyncio.sleep(1)
    #await message.reply("Gostaria de me enviar arquivos? " , reply_markup=markup)
    await message.reply("Gostaria de me enviar um arquivo? " , reply_markup=markup)
@dp.message_handler(state=Form.escolha)
async def process_escolha (message: types.Message, state: FSMContext):
    """
    Processa a escolha
    """
    if message.text == "SIM":
        #await message.reply("Envie documentos de forma agrupada. (PDF, Word, Excel)")
        await message.reply("Envie Um documento (PDF, Word, Excel)")
    else:
        await types.ChatActions.typing()
        await asyncio.sleep(2)
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("SIM", "NÃO")
        await types.ChatActions.typing()
        await asyncio.sleep(1)
        #await message.reply("Gostaria de me enviar imagens? " , reply_markup=markup)
        await message.reply("Gostaria de me enviar uma imagem? " , reply_markup=markup)

        arq64 =  []
        await Form.next()
    await Form.next()
    @dp.message_handler(state=Form.arq, content_types=ContentTypes.DOCUMENT)
    async def process_arq (message: types.Message, state: FSMContext):
        global encoded
        global mime
        global attach
        global quebra
        global arq64
        global last
        last = quebra
        await types.ChatActions.typing()
        attach = "/documents/"
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("SIM", "NÃO")
        await types.ChatActions.typing()
        """
        Processa o arquivo
        """
        await message.document.download(
            destination_dir=doc,
        )
        await doarq()
        if quebra == 1:
            print("arquivo")
        if quebra == 1:
            await Form.next()
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("SIM", "NÃO")
            await types.ChatActions.typing()
            await asyncio.sleep(1)
        #await message.reply("Gostaria de me enviar imagens? " , reply_markup=markup)
        await message.reply("Gostaria de me enviar uma imagem? " , reply_markup=markup)
@dp.message_handler(state=Form.escfoto)
async def process_escfoto (message: types.Message, state: FSMContext):
    """
    Processa a escolha
    """
    if message.text == "SIM":
        #await message.reply("Envie as imagens agrupadas.")
        await message.reply("Envie Uma imagem")
    else:
        await types.ChatActions.typing()
        possible_responses = [
            #("Gostaria de me enviar Videos? ")
            ("Gostaria de me enviar um Video? ")
        ]
        resposta =  random.choice(possible_responses)
        await message.reply(resposta)
        img64 = []
        await Form.next()
    types.ReplyKeyboardRemove()
    await Form.next()
@dp.message_handler(state=Form.foto, content_types=['photo'])
async def process_foto (message: types.Message, state: FSMContext):
    global encodedimage
    global mimeimage
    global img64
    global attach
    global quebra
    await types.ChatActions.typing()
    possible_responses = [
            #("Gostaria de me enviar Videos? ")
            ("Gostaria de me enviar um Video? ")
    ]
    resposta =  random.choice(possible_responses)
    """
    Processa o arquivo
    """
    await message.photo[-1].download(destination_dir=doc)
    attach = "/photos/"
    await doimage()

    if quebra == 1:
        print("photo")
    
    if quebra == 1:
        await message.reply(resposta)
        await Form.next()
        types.ReplyKeyboardRemove()
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("SIM", "NÃO")
        await types.ChatActions.typing()
        await asyncio.sleep(1)
        #await message.reply("Gostaria de me enviar Videos? " , reply_markup=markup)
        await message.reply("Gostaria de me enviar um Video? " , reply_markup=markup)





@dp.message_handler(state=Form.escvideo)
async def process_escvideo (message: types.Message, state: FSMContext):
    """
    Processa a escolha
    """
    if message.text == "SIM":
        #await message.reply("Envie Os videos de forma agrupada.")
        await message.reply("Envie Um video ")
    else:
        await types.ChatActions.typing()
        possible_responses = [
            ("Perfeito, voce pode me falar mais informações sobre o que aconteceu? Fique a vontade para escrever em detalhes ok?"),
            ("Obrigado, agora, se você puder me informar mais sobre o ocorrido, pode ficar a vontade para escrever em detalhes."),
            ("Acho que entendi, me fala mais sobre o que está acontecendo. Pode descrever em detalhes."),
            ("tudo bem, agora eu preciso que você me dê mais informações, uma descrição completa e em detalhes do que está ocorrendo."),
            ("OK! você poderia me falar mais sobre isso? Pode escrever em detalhes.")
        ]
        resposta =  random.choice(possible_responses)
        await message.reply(resposta)
        img64 = []
        await Form.next()
    types.ReplyKeyboardRemove()
    await Form.next()
@dp.message_handler(state=Form.video, content_types=['video'])
async def process_video (message: types.Message, state: FSMContext):
    global encodedimage
    global mimeimage
    global img64
    global attach
    global quebra
    await types.ChatActions.typing()
    await asyncio.sleep(2)
    possible_responses = [
        ("Perfeito, voce pode me falar mais informações sobre o que aconteceu? Fique a vontade para escrever em detalhes ok?"),
        ("Obrigado, agora, se você puder me informar mais sobre o ocorrido, pode ficar a vontade para escrever em detalhes."),
        ("Acho que entendi, me fala mais sobre o que está acontecendo. Pode descrever em detalhes."),
        ("tudo bem, agora eu preciso que você me dê mais informações, uma descrição completa e em detalhes do que está ocorrendo."),
        ("OK! você poderia me falar mais sobre isso? Pode escrever em detalhes.")
    ]
    resposta =  random.choice(possible_responses)
    """
    Processa o arquivo
    """
    await message.video.download(destination_dir=doc)
    attach = "/videos/"      
    if quebra == 1:
        quebra = 1
    await doimage()

    if quebra == 1:
        print("video")
    
    if quebra == 1:
        await message.reply(resposta)
        await Form.next()
        types.ReplyKeyboardRemove()





@dp.message_handler(state=Form.obs)
async def process_obs(message: types.Message, state: FSMContext):
    """
    Processa a obs
    """
    async with state.proxy() as data:
        data['obs'] = message.text
        print("obs")
        if len(data['obs']) > 20:
            global json_dump
            global mot
            global img64
            global arq64
            global userid
            global clientid
            global imagem
            global arquivo
            if data['motivo'] == "1":
                mot = 2
            elif data['motivo'] == "2":
                mot = 3
            elif data['motivo'] == "3":
                mot = 4
            elif data['motivo'] == "4":
                mot = 5
            elif data['motivo'] == "5":
                mot = 15
            elif data['motivo'] == "6":
                mot = 16
            motivo = mot
            nome = data['nome']
            #nome = nome.encode('utf-8')
            desc = data['desc'].upper()
            #desc = desc.encode('utf-8')
            obs = data['obs']
            #obs = obs.encode('utf-8')
            prio = 2
            fone = data['fone']
            email = data['email']
            soft = data['software']
            mod = data['modulo']
            count = 0
            print (arq64)
            print(mime)
            print(img64)
            print(mimeimage)
            for a in arq64:
                arquivo.append({"base64":  arq64[count]  , "extencao":  mime[count]  },)
                if arq64 == []:
                    arquivo.append('')
                count = count + 1
            count = 0
            for i in img64:
                imagem.append({"base64": img64[count] , "extencao" :  mimeimage[count]  },)
                if img64 == []:
                    imagem.append('')
                count = count + 1
            jso = arquivo, imagem
            jj = str(jso)
            jj = jj.replace("[", "")
            jj = jj.replace("]", "")
            arq = {"sdtInserirChamado": {"FormaChamadoId": 10, "MotivoChamadoId": motivo, "AtendenteId": 107,
                                         "ClienteId": clientid, "ChamadoSolicitante": nome,
                                         "ChamadoEnderecoResposta": email, "SoftwareId": soft, "ModuloId": mod,
                                         "ChamadoDescricao": desc, "ChamadoDescricaoCompleta": obs,
                                         "PrioridadeId": prio, "ChamadoFoneRetorno": fone, "ChamadoIdExterno": 0,
                                         "ChamadoMensagemMensagem": "", "UsuarioId": userid,
                                         "anexos": [jj]}}
            #arquivo = open('log.tegabot', 'w')
            json_dump = str(arq)
            json_dump = json_dump.replace("'", '"')
            json_dump = json_dump.replace('],[', ',')
            json_dump =json_dump.replace(',"]]}', ']}')
            json_dump = json_dump.replace('{[[', '{[')
            json_dump = json_dump.replace('[[', ',[')
            json_dump = json_dump.replace(']]', ']')
            json_dump = json_dump.replace('"(', "")
            json_dump = json_dump.replace(')"', "")
            print(json_dump)
            file = open('json.json', 'w')
            file.write(json_dump)
            file.close()
            await types.ChatActions.typing()
            await asyncio.sleep(1)
            if data['motivo'] == "1":
                data['motivo'] =  "Dúvida/Erro de usuário"
            elif data['motivo'] == "2":
                data['motivo'] = "Solicitação de especifíco (Módulo novo)"
            elif data['motivo'] == "3":
                data['motivo'] =  "Solicitação de melhoria"
            elif data['motivo'] == "4":
                data['motivo'] = "Erro de sistema"
            elif data['motivo'] == "5":
                data['motivo'] = "Instalação/Atualização"
            elif data['motivo'] == "6":
                data['motivo'] = "Atendimento"
            await bot.send_message(
                message.chat.id,
                md.text(
                    md.text('Dados do chamado'),
                    md.text('Nome: ', md.bold(data['nome'])),
                    md.text('Motivo: ', md.bold(data['motivo'])),
                    md.text('Descrição: ', md.bold(data['desc'])),
                    md.text('Prioridade:', md.bold(data['prio'])),
                    md.text('Obs:', md.bold(data['obs'])),
                    md.text(''),
                    sep='\n',
                ),
                parse_mode=ParseMode.MARKDOWN,
            )
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("SIM", "NÃO")
            await types.ChatActions.typing()
            await asyncio.sleep(1)
            await message.reply("Deseja confirmar a abertura do chamado? ", reply_markup=markup)
            await Form.next()
        else:
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("/start")
            await types.ChatActions.typing()
            await asyncio.sleep(1)
            await message.reply("Dados insuficientes informados para o campo: 'Descrição completa', para tentar novamente clique no botão abaixo ou me envie o comando /start",reply_markup=markup)
            await state.finish()
@dp.message_handler(state=Form.confirm)
async def process_confirm(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['confirm'] = message.text
        print("confirm")
    if(data['confirm'] == "SIM"):
        global json_dump
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("/start")
        url = link + 'InserirChamado'
        myobj = json_dump 
        headers = {'Content-type': 'application/json'}
        x = requests.post(url, headers=headers, data = myobj, timeout=2)
        stats = 200
        print(x.text)
        print(x.status_code)
        if x.status_code == stats:
            formater = x.text
            formater = formater.replace('{"Body":"', "")
            formater = formater.replace('"}', " ")
            cod = formater[:-3]
            await types.ChatActions.typing()
            await asyncio.sleep(1)
            await message.reply("Tudo certo, o chamado foi aberto com sucesso no código: " + cod, reply_markup=markup)
        else:
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("/start")
            await types.ChatActions.typing()
            await asyncio.sleep(1)
            await message.reply("Houve um erro na solicitação, para tentar novamente clique no botão abaixo ou me envie o comando /start", reply_markup=markup)
        await state.finish()
    elif(data['confirm'] == "NÃO"):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("/start")
        await types.ChatActions.typing()
        await asyncio.sleep(1)
        await message.reply("Solicitação cancelada, para tentar novamente clique no botão abaixo ou me envie o comando /start", reply_markup=markup)
        # Finish conversation
        await state.finish()

async def doarq():
    global quebra
    global last
    last = quebra
    i = 0
    lista = []
    lista = os.listdir(doc + attach)
    for l in lista:
        valida = len(os.listdir(doc + attach))
        filename = l
        file = doc + attach + filename
        with open(file, "rb") as file_binary:
            data = file_binary.read()
            encoded = base64.b64encode(data)
            encoded_utf8 = encoded.decode('utf-8')
            arq64.append(encoded_utf8)
            mine = base64.b64decode(encoded)
            tipo = filename.split(".")
            mime.append(tipo[1])
#        print(file)
#        print(quebra)
#        valida = len(os.listdir(doc + attach))
 #       print(valida)
#       if not valida:
#            quebra = 1
    for l in lista:
        filename = l
        file = doc + attach + filename
        os.remove(file)
#        valida = len(os.listdir(doc + attach))
#        print(valida)
#        if quebra == 1:
#           valida = 1
#       if not valida:
#            quebra = 1
##        if last ==1 & quebra == 0:
 #           quebra = 0
 #       if last == quebra:
 #           quebra = 0
 #       if quebra == 1 & last ==1:
 #           quebra = 1
 #       if quebra ==0 & last == 1:
 #           quebra =0
 #       if quebra == last:
        quebra = 1
async def doimage():
    global quebra
    global last
    last = quebra
    i = 0
    lista = []
    lista = os.listdir(doc + attach)
    for l in lista:
        filename = l
        file = doc + attach + filename
        with open(file, "rb") as file_binary:
            data = file_binary.read()
            encoded = base64.b64encode(data)
            encoded_utf8 = encoded.decode('utf-8')
            img64.append(encoded_utf8)
            mine = base64.b64decode(encoded)
            tipo = filename.split(".")
            mimeimage.append(tipo[1])
        print(file)
        print(quebra)
        valida = len(os.listdir(doc + attach))
        print(valida)
#        if not valida:
#           quebra = 1
    for l in lista:
        filename = l
        file = doc + attach + filename
        os.remove(file)
        valida = len(os.listdir(doc + attach))
        print(valida)
#        if quebra == 1:
#            valida = 1
#        if not valida:
#            quebra = 1
#        if last ==1 & quebra == 0:
#            quebra = 0
#        if last == quebra:
#            quebra = 0
#        if quebra == 1 & last ==1:
#            quebra = 1
#        if quebra ==0 & last == 1:
#            quebra =0
#        if quebra == last:
        quebra = 1
if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
